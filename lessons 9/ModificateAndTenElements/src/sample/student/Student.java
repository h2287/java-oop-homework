package sample.student;

import java.util.Objects;

import sample.Human;
import sample.enums.Gender;

public class Student extends Human {

	private int id;
	private String groupName;

	public Student(String name, String lastname, Gender gender, int id, String groupName) {
		super(name, lastname, gender);
		this.id = id;
		this.groupName = groupName;
	}

	public Student(String name, String lastname, Gender gender, int id) {
		super(name, lastname, gender);
		this.id = id;
		this.groupName = "";
	}

	public Student() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", groupName=" + groupName + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getId(), getGroupName());

	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		if (!super.equals(obj))
			return false;
		Student student = (Student) obj;
		return Objects.equals(getGroupName(), student.getGroupName()) && getId() == student.getId();
	}

}
