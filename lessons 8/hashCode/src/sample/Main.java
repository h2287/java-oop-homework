package sample;

import sample.enums.Gender;
import sample.exaptions.GroupOverflowException;
import sample.student.Student;

public class Main {

	public static void main(String[] args) {

		String groupName = "Builders";
		Group group = new Group(groupName);

		Student student10 = new Student("Man", "Spider", Gender.SPIDER_MAN, 11, groupName);

		addStudentToGroup(group, new Student("Nestr", "Petrenko", Gender.MALE, 1, groupName));
		addStudentToGroup(group, new Student("Oleksandr", "Andrienko", Gender.MALE, 2, groupName));
		addStudentToGroup(group, new Student("Konstantin", "Petruk", Gender.MALE, 3, groupName));
		addStudentToGroup(group, new Student("Jenny ", "Andriuk", Gender.FEMALE, 4, groupName));
		addStudentToGroup(group, new Student("Bohdan", "Petrych", Gender.MALE, 5, groupName));
		addStudentToGroup(group, new Student("Vanessa", "Andrievych", Gender.FEMALE, 6, groupName));
		addStudentToGroup(group, new Student("Natalia", "Petrivna", Gender.FEMALE, 7, groupName));
		addStudentToGroup(group, new Student("Sveta", "Andriivna", Gender.FEMALE, 8, groupName));

		addStudentToGroup(group, new Student("Man", "Spider", Gender.SPIDER_MAN, 11, groupName));
		System.out.println(group.areThereEquivalentStudents());

		addStudentToGroup(group, new Student("Man", "Spider", Gender.SPIDER_MAN, 11, groupName));
		System.out.println(group.areThereEquivalentStudents());

	}

	private static void addStudentToGroup(Group group, Student student) {
		try {
			group.addStudent(student);
		} catch (GroupOverflowException e) {
			System.out.println(e.getMessage());
		}
	}

	private static void printGroupInfo(Group group) {
		System.out.println("separator(----------)separator");
		System.out.println(group);
		System.out.println("separator(----------)separator");
	}

}
