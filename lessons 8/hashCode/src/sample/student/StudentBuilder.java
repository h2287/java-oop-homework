package sample.student;

import java.util.InputMismatchException;
import java.util.Scanner;

import sample.Group;
import sample.enums.Gender;
import sample.exaptions.GroupOverflowException;

public class StudentBuilder {

	public static Student createStudent() {

		Scanner sc = new Scanner(System.in);

		System.out.println("Add new student: ");
		System.out.println(" Input name: ");
		String name = sc.nextLine();

		System.out.println(" Input last name: ");
		String lastName = sc.nextLine();

		System.out.println(" Input gender (male or female): ");
		String newGender = sc.nextLine().toUpperCase();
		Gender gender;

		try {
			gender = Gender.valueOf(newGender);
		} catch (IllegalArgumentException e) {
			gender = Gender.SPIDER_MAN;
		}

		int id = 0;
		while (id <= 0) {
			try {
				System.out.println(" Input ID number: ");
				id = sc.nextInt();
			} catch (InputMismatchException e) {
				sc.next();
				System.out.println(" Wrong input format. Input correct number.");
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return new Student(name, lastName, gender, id);

	}

	public static void addToGroup(Student student, Group group) throws GroupOverflowException {
		group.addStudent(student);
	}

}
