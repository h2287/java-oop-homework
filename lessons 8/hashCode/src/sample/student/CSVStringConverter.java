package sample.student;

import sample.enums.Gender;

public class CSVStringConverter implements StringConverter {

	@Override
	public String toStringRepresentation(Student student) {
		// TODO Auto-generated method stub
		return student.getName() + "," + student.getLastName() + "," + student.getGender() + "," + student.getId() + ","
				+ student.getGroupName() + ".";
	}

	@Override
	public Student fromStringRepresentation(String str) {
		// TODO Auto-generated method stub
		Student student = new Student();
		String[] studentAttribut = str.split(",");
		student.setName(studentAttribut[0]);
		student.setLastName(studentAttribut[1]);
		student.setGender(Gender.valueOf(studentAttribut[2]));
		student.setId(Integer.parseInt(studentAttribut[3]));
		student.setGroupName(studentAttribut[4]);
		return student;
	}

}
