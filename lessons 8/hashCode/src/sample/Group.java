package sample;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

import sample.comparators.StudentLastNameComparator;
import sample.exaptions.GroupOverflowException;
import sample.exaptions.StudentNotFoundException;
import sample.student.Student;

public class Group {

	private Student[] students = new Student[10];
	private String groupName;

	public Group(Student[] students, String groupName) {
		super();
		this.students = students;
		this.groupName = groupName;
	}

	public Group(String groupName) {
		super();
		this.groupName = groupName;

	}

	public Group() {
		super();
	}

	public static void sortStudentsByLastName(Student[] students) {
		Arrays.sort(students, Comparator.nullsLast(new StudentLastNameComparator()));
	}

	public Student[] getStudents() {
		return students;
	}

	public void setStudents(Student[] students) {
		this.students = students;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public void addStudent(Student student) throws GroupOverflowException {
		for (int i = 0; i < students.length; i++) {
			if (this.students[i] != null && i == this.students.length - 1) {
				throw new GroupOverflowException(student.getName() + " " + student.getLastName()
						+ " cannot be added to the " + this.groupName + " group as it is full!");
			} else if (this.students[i] == null) {
				student.setGroupName(this.groupName);
				this.students[i] = student;
				System.out.println(
						student.getName() + " " + student.getLastName() + " added to " + this.groupName + " group.");
				break;
			}
		}
	}

	public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {
		Student foundStudent = null;
		for (Student student : this.students) {
			if (student != null && student.getLastName().equals(lastName)) {
				foundStudent = student;
			}
		}
		if (foundStudent == null) {
			throw new StudentNotFoundException(
					"Student with last name " + lastName + " not found " + this.groupName + " group.");
		}
		return foundStudent;
	}

	public boolean removeStudentByID(int id) {
		for (int i = 0; i < students.length; i++) {
			if (this.students[i] != null && this.students[i].getId() == id) {
				this.students[i] = null;
				System.out.println("Student with id card " + id + " is removed from group " + this.groupName);
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		sortStudentsByLastName(students);
		StringBuilder sb = new StringBuilder();

		sb.append("Group: ");
		sb.append(" groupName= ").append(groupName).append("\n");

		for (Student student : this.students) {
			sb.append("   ").append(student != null ? student.toString() : "null").append("\n");
		}

		return sb.toString();
	}

	public boolean areThereEquivalentStudents() {
		for (int i = 0; i < students.length - 1; i++) {
			for (int j = 0; j < students.length; j++) {
				if (students[i] != null && students[i].equals(students[j])) {
					return true;
				}

			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getGroupName(), Arrays.hashCode(getStudents()));
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group group = (Group) obj;
		return Objects.equals(getGroupName(), group.getGroupName())
				&& Arrays.equals(getStudents(), group.getStudents());
	}

}
