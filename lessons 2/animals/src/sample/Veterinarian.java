package sample;

public class Veterinarian {
	private String name;

	public Veterinarian(String name) {
		super();
		this.name = name;
	}

	public Veterinarian() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Veterinarian [name = " + name + "]";
	}

	public void treatmant(Animal animal) {
		System.out.println("Я ветеринар " + name);
		System.out.println("Я лікую тварину " + animal.toString());

	}

}
