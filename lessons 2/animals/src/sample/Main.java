package sample;

public class Main {

	public static void main(String[] args) {
		Cat cat1 = new Cat("milk", "green", 4, "Barsik");
		Dog dog1 = new Dog("meat", "yellow", 6, "Rex");
		Veterinarian vet1 = new Veterinarian("Illia");

		System.out.println(cat1);
		System.out.println(dog1);
		System.out.println(vet1);

		vet1.treatmant(cat1);
		vet1.treatmant(dog1);
	}

}
