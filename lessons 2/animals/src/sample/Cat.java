package sample;

public class Cat extends Animal {

	private String name;

	public Cat(String retion, String color, int weight, String name) {
		super(retion, color, weight);
		this.name = name;
	}

	public Cat(String retion, String color, int weight) {
		super(retion, color, weight);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Cat [name = " + name + super.toString() + "]";
	}

	@Override
	public String getVoice() {
		// TODO Auto-generated method stub
		return "Mya";
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		System.out.println("cat eats ");
	}

	@Override
	public void sleep() {
		// TODO Auto-generated method stub
		System.out.println("cat sleeps ");
	}

}
