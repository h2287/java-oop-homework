package sample;

public class Animal {

	private String retion;
	private String color;
	private int weight;

	public Animal(String retion, String color, int weight) {
		super();
		this.retion = retion;
		this.color = color;
		this.weight = weight;
	}

	public Animal() {
		super();
	}

	public String getRetion() {
		return retion;
	}

	public void setRetion(String retion) {
		this.retion = retion;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return " Animal [retion = " + retion + ", color = " + color + ", weight = " + weight + "]";
	}

	public String getVoice() {
		return "voice";
	}

	public void eat() {
		System.out.println("animal eats");
	}

	public void sleep() {
		System.out.println("animal sleep");
	}

}
