package sample;

public class Dog extends Animal{

	private String name;

	public Dog(String retion, String color, int weight, String name) {
		super(retion, color, weight);
		this.name = name;
	}

	public Dog(String retion, String color, int weight) {
		super(retion, color, weight);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Dog [name = " + name + super.toString() + "]";
	}

	@Override
	public String getVoice() {
		// TODO Auto-generated method stub
		return "gav";
	}

	@Override
	public void eat() {
		// TODO Auto-generated method stub
		System.out.println("dog eats ");
	}

	@Override
	public void sleep() {
		// TODO Auto-generated method stub
		System.out.println("dog sleeps ");
	}
	
	
	
}
