package sample;

import java.io.File;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter text: ");
		String text = sc.nextLine();

		TextTransformer text1 = new TextTransformer();
		System.out.println(text1.transform(text));

		InverseTransformer text2 = new InverseTransformer();
		System.out.println(text2.transform(text));

		UpDownTransformer text3 = new UpDownTransformer();
		System.out.println(text3.transform(text));

		File file = new File("text.txt");
		TextSave sv = new TextSave(text3, file);
		sv.saveTextToFile(text);
	}

}
