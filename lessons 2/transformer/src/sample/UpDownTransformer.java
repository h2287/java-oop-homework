package sample;

import java.util.Arrays;

public class UpDownTransformer extends TextTransformer {

	public String transform(String text) {
		String result = "";
		for (int i = 0; i < text.length(); i++)
			if (i % 2 == 0)
				result += Character.toString(text.charAt(i)).toLowerCase();
			else
				result += Character.toString(text.charAt(i)).toUpperCase();

		return result;
	}
}