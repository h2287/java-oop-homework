package copyFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CopyService {

	public static long copyFile(File fileIn, File fileOut) throws IOException {
		long totalBytes;
		try (InputStream is = new FileInputStream(fileIn); OutputStream os = new FileOutputStream(fileOut)) {
			totalBytes = is.transferTo(os);
		}
		return totalBytes;
	}

	public static long copyFolder(File folderIn, File folderOut, String ext) throws IOException {
		File[] files = folderIn.listFiles();
		long totalBytes = 0;
		if (files != null) {
			for (File file : files) {
				if (file.isFile() && (file.getName().endsWith(ext))) {
					File fileOut = new File(folderOut, file.getName());
					totalBytes += copyFile(file, fileOut);
				}
			}
		}
		return totalBytes;
	}

}
