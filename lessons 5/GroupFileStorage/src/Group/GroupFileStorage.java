package Group;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import sample.student.CSVStringConverter;
import sample.student.Student;

public class GroupFileStorage {

	public void saveGroupToCSV(Group group) throws IOException {
		File file = new File(group.getGroupName() + ".csv");
		Student[] students = group.getStudents();
		CSVStringConverter csv = new CSVStringConverter();

		try (FileWriter fw = new FileWriter(file)) {
			for (Student student : students) {
				if (students != null) {
					fw.append(csv.toStringRepresentation(student)).append(System.lineSeparator());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Group loadGroupFromCSV(File file) throws IOException {
		Group group = new Group(file.getName().substring(0, file.getName().lastIndexOf(".")));
		Student[] students = group.getStudents();
		CSVStringConverter csv = new CSVStringConverter();

		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			for (int i = 0; i < students.length; i++) {
				String studentCSV = reader.readLine();
				if (studentCSV != null) {
					students[i] = csv.fromStringRepresentation(studentCSV);
				}
			}
		} catch (IOException e) {
			return null;

		}
		group.setStudents(students);
		return group;
	}

	public File findFileByGroupName(String groupName, File workFolder) {
		File[] files = workFolder.listFiles();

		if (files != null) {
			for (File file : files) {
				if (file.isFile() && file.getName().equals(groupName + ".csv")) {
					return file;
				}
			}
		}
		return null;
	}

}
