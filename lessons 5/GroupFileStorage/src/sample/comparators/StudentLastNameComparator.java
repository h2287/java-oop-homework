package sample.comparators;

import java.util.Comparator;

import sample.Human;

public class StudentLastNameComparator implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {

		Human student1 = (Human) o1;
		Human student2 = (Human) o2;

		String lastName1 = student1.getLastName();
		String lastName2 = student2.getLastName();

		if (lastName1.compareTo(lastName2) > 0) {
			return 1;
		}

		if (lastName1.compareTo(lastName2) < 0) {
			return 1;
		}
		return 0;
	}

}
