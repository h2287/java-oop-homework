package sample.student;

public interface StringConverter {

	public String toStringRepresentation(Student student);

	public Student fromStringRepresentation(String str);

}
