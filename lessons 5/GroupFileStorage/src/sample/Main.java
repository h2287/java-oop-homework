package sample;

import java.io.File;
import java.io.IOException;

import javax.management.RuntimeErrorException;

import Group.Group;
import Group.GroupFileStorage;
import sample.enums.Gender;
import sample.exaptions.GroupOverflowException;
import sample.student.Student;
import sample.student.StudentBuilder;

public class Main {

	public static void main(String[] args) {

		String groupName = "Builders";
		Group group = new Group(groupName);

		Student student01 = new Student("Nestr", "Petrenko", Gender.MALE, 1, groupName);
		Student student02 = new Student("Oleksandr", "Andriienko", Gender.MALE, 2, groupName);
		Student student03 = new Student("Konstantin", "Petruk", Gender.MALE, 3, groupName);
		Student student04 = new Student("Jenny ", "Andriiuk", Gender.FEMALE, 4, groupName);
		Student student05 = new Student("Bohdan", "Petrych", Gender.MALE, 5, groupName);
		Student student06 = new Student("Vanessa", "Andriievych", Gender.FEMALE, 6, groupName);
		Student student07 = new Student("Natalia", "Petriv", Gender.FEMALE, 7, groupName);
		Student student08 = new Student("Sveta", "Andriiv", Gender.FEMALE, 8, groupName);
		Student student09 = new Student("Petro", "Petrash", Gender.MALE, 9, groupName);
		Student student10 = new Student("Oleh", "Andriiash", Gender.MALE, 10, groupName);

		addStudentToGroup(group, student01);
		addStudentToGroup(group, student02);
		addStudentToGroup(group, student03);
		addStudentToGroup(group, student04);
		addStudentToGroup(group, student05);
		addStudentToGroup(group, student06);
		addStudentToGroup(group, student07);
		addStudentToGroup(group, student08);
		addStudentToGroup(group, student09);
		addStudentToGroup(group, student10);

		printGroupInfo(group);

		GroupFileStorage gfs = new GroupFileStorage();

		try {
			gfs.saveGroupToCSV(group);
		} catch (IOException e) {
			e.printStackTrace();
		}

		File file = new File("./Builders.csv");
		Group group2;
		try {
			group2 = gfs.loadGroupFromCSV(file);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		printGroupInfo(group2);

		File workFolder = new File(".");
		System.out.println(gfs.findFileByGroupName("Builders", workFolder));

	}

	private static void printGroupInfo(Group group) {
		System.out.println("separator(----------)separator");
		System.out.println(group);
		System.out.println("separator(----------)separator");
	}

	private static void addStudentToGroup(Group group, Student student) {
		try {
			StudentBuilder.addToGroup(student, group);
		} catch (GroupOverflowException e) {
			System.out.println(e.getMessage());
		}
	}

}
