package sample;

public class Main {

	public static void main(String[] args) {
		Network net1 = new Network("SpeedNet");

		Phone p1 = new Phone(1234);
		p1.Registration(net1);

		Phone p2 = new Phone(985);
		p2.Registration(net1);

		p1.Call(985);
		p1.Call(345);
	}

}
