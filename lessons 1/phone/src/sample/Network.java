package sample;

import java.util.ArrayList;

public class Network {

	private String Name;

	private ArrayList<Phone> PhoneList;

	public Network(String name) {
		super();
		Name = name;
		PhoneList = new ArrayList<Phone>();
	}

	public Network() {
		super();
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	

	public void AddPhone(Phone phone) {
		PhoneList.add(phone);
	}

	public Phone getPhoneByNumber(int Number) {
		Phone result = null;
		for (Phone i : PhoneList) {
			if (Number == i.getNumber())
				result = i;
			break;
		}
		return result;
	}

	@Override
	public String toString() {
		return "Network [Name=" + Name + ", PhoneList=" + PhoneList + "]";
	}

}
