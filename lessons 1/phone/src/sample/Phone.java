package sample;

public class Phone {
	private int Number;
	private Network network;

	public Phone(int number) {
		super();
		Number = number;
	}

	public Phone() {
		super();
	}
	
	public void Call (int Number) {
		Phone phone2=network.getPhoneByNumber(Number);
		if(phone2!=null) {
			System.out.println("Number: "+this.Number+" call on "+Number+".");
			phone2.Incoming(this.Number);
		}else {
			System.out.println("Number "+Number+" not registrate in web.");
		}
		return;
	}
	
	public void Registration(Network network) {
		this.network=network;
		network.AddPhone(this);
	}
	
	public void Incoming(int Number) {
		System.out.println("You get call of "+Number+" on your phone "+this.Number+".");
	}

	public int getNumber() {
		return Number;
	}

	public void setNumber(int number) {
		Number = number;
	}

	public Network getNetwork() {
		return network;
	}

	public void setNetwork(Network network) {
		this.network = network;
	}

	@Override
	public String toString() {
		return "Phone [Number=" + Number + ", network=" + network + "]";
	}
	
	

}
