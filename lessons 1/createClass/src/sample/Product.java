package sample;

public class Product {

	private double cost;
	private double weight;
	private String description;

	public Product(double cost, double weight, String description) {
		super();
		this.cost = cost;
		this.weight = weight;
		this.description = description;
	}

	public Product() {
		super();
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Product [cost=" + cost + ", weight=" + weight + ", description=" + description + "]";
	}

}
